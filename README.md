

# Ansible проект для развертывания Kubernetes кластера

Этот проект содержит Ansible плейбуки для автоматизации развертывания Kubernetes кластера. Включает в себя настройку control plane ноды, worker нод и установку необходимых зависимостей.

## Структура проекта

- `hosts`: Инвентарный файл, содержащий описание хостов для развертывания.
- `k8s-dependencies.yml`: Плейбук для установки зависимостей Kubernetes на все ноды.
- `k8s-control-plane.yml`: Плейбук для инициализации control plane ноды.
- `k8s-workers.yml`: Плейбук для присоединения worker нод к кластеру.

## Предварительные требования

- Ansible 2.9 или выше.
- Установленный Python 3 на управляющей машине и всех целевых хостах.
- SSH доступ к целевым хостам под пользователем `ansible`.

## Настройка инвентарного файла

Инвентарный файл `hosts` должен быть настроен в соответствии с вашей инфраструктурой:

```ini
[control_plane]
control1 ansible_host=control_plane_ip ansible_user=ansible 

[workers]
worker1 ansible_host=worker_1_ip ansible_user=ansible
worker2 ansible_host=worker_2_ip ansible_user=ansible

[all:vars]
ansible_python_interpreter=/usr/bin/python3
```

## Использование

1. Установите все необходимые зависимости на все ноды:

```bash
ansible-playbook -i hosts k8s-dependencies.yml
```

2. Инициализируйте Kubernetes control plane ноду:

```bash
ansible-playbook -i hosts k8s-control-plane.yml
```

3. Присоедините worker ноды к кластеру:

```bash
ansible-playbook -i hosts k8s-workers.yml
```

## Важно

- Убедитесь, что у вас есть права суперпользователя (`become: yes`) для выполнения задач, требующих повышения прав.
- Проверьте IP адреса и пользователей в инвентарном файле `hosts`, они должны соответствовать вашей инфраструктуре.
- Все плейбуки тестировались на Ubuntu 18.04 и 20.04. При использовании других дистрибутивов возможна необходимость внесения изменений.

После успешного выполнения всех плейбуков, ваш Kubernetes кластер будет готов к использованию.